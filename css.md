# CSS (cascading stylesheets)

## FLEXBOX

```css
display: flex | inline-flex
flex-direction: row | column
flex-wrap: wrap | nowrap | wrapreverse
flex basis: <length>
justify-content: flex-start | flex-end | center
align-self: flex-start | flex-end | center
align-items: flex-start | flex-end | center
align-content: flex-start | flex-end | center
flex-grow: <number>
flex-shrink: <number>
flex: <integer>
order: <integer> # dont mess with the order!
```




### Resources

- [mdn](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox)
- [css-tricks](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [interactive demo](https://yoksel.github.io/flex-cheatsheet/)
- [The Cascade](https://wattenberger.com/blog/css-cascade)


### Interactive games 

* [Flexbox Froggy](https://flexboxfroggy.com/)
* [CSS Diner](https://flukeout.github.io/)
* [CSS SpeedRun](https://css-speedrun.netlify.app/)
* [Grid Attack](https://codingfantasy.com/games/css-grid-attack)
* [Flexbox Zombies](https://mastery.games/flexboxzombies/)
