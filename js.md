# Javascript

## The Event Loop

### js and rendering

* Avoid setTimeout or setInterval for visual updates; always use requestAnimationFrame instead.
* Move long-running JavaScript off the main thread to Web Workers.
* Use micro-tasks to make DOM changes over several frames.
*

---
## Notes

* terminology:

	* syntax parsers
	* lexical environment (environment record +  reference to the outer env) (related to closures)
	* execution context  (every exec context has its own lexical env)
	* execution stack 
	* scope chain
	* event queue
	* event loop ("exec stack" empty => check "event queue")


* after the execution stack is empty, then an execution context is created for every event
js wont look at the event queue until the stack is empty




* 6 primitive types: undefined, null, boolean, number, string, symbol(es6)

* [operator precedence](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_Precedence)

* coercion

* [equality comparision and sameness](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Equality_comparisons_and_sameness)

* [short circuit evaluation || ](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_OR) (was common pattern for defalut value)(before default params were a thing)


* [nullish coalescing operator ??](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator) helps filtering null & undefined

* first class functions (functions are first class objects):

		A programming language is said to have First-class functions when functions in that language are treated like any other variable. For example, in such a language, a function can be passed as an argument to other functions, can be returned by another function and can be assigned as a value to a variable.

* Closures & Callbacks

		A closure is the combination of a function bundled together (enclosed) with references to its surrounding state (the lexical environment).

* "arguments" object (non-arrow funcs) === "...rest" parameter (arrow funcs)

* function overloading / function factories (theyre not the same)

* IIFE's


* call(), apply(), bind() and function currying

* functional programming

* CJS(CommonJS), AMD(Asynchronous Module Definition), UMD(Universal Module Definition), and ESM(ES Modules
)

* Prototype chain / prototypal inheritance / reflection & extending

* Function cunstroctor


---

## must be familiar with:

- faking namespaces (using objects)
- function statement vs function expression
- transpilation & polyfill & linting

## Resources & useful links

* [promises are friendly](https://developers.google.com/web/fundamentals/primers/async-functions)