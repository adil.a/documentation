# git version control system

The way we'll be working most times is that we'll be replacing the `master` or `main` with the `develop` branch using the following command.

```powershell
git branch (-m | -M) [<oldbranch>] <newbranch>
git branch -M develop     # -M (shortcut for --move --force)
git branch -D main
git push origin --delete main #(might not work if the branch is protected)
```

Next we push to github/gitlab and we make develop the "base" branch, and make it protected.

Also, you'll need to remove protection for the default branch (if there is any) before deleting it.

- gitlab:
  - `repo -> setting -> repository -> Protected branches`
  - `repo -> setting -> repository -> Default Branch`
- github: (branches protection disabled by default && pro feature)
  - `repo -> settings -> branches -> default branch -> double arrow symbol`

---

---

## Cheat Sheet

```powershell
# the obvious ones first
git init
git status
git restore <file>			# discard unstaged changes in file
git restore . 				# discard unstaged changes in all tracked fils
git add .					#adds all files, you can specify by name(s)
git commit -m "commit message"
git push

### push ###
git push origin HEAD				# push the current branch to the same name on the remote
git push origin branchName			# push branchName branch to the remote
git push --all origin				# push all branches to remote

### branch ###
git branch 								# list local branches
git branch branchName3 					# cretes a branch called branchName3
git branch -a 							# list all branches, local and remote
git branch -d localBranch1				# delete localbranch1
git push origin --delete remoteBranch1	# delete remoteBranch1 remotely

### checkout ###
git checkout branchName				#chekout to an existing branch
git checkout 3f4e987				# checkout to the commit with the hash 3f4e987
git checkout -b newFeature			# create newFeature branch and checkout to it
git checkout -b newFeature master	# creates the new feature branch from master
git checkout -						# quickly check out to the last branch you were at

### - ###
git config --global alias.lol "log --oneline --graph --decorate" # adding an alias "git lol"

### log ###
git log branchName # commit history for branchName branch
git log --oneline --graph --decorate  # pretty console graph output

###------working w/ remote branches------###
git remote -v # shows remotes for fetch and push 
# you might have multiple remotes, main repo(origin) and your fork for example

git branch -a # duh
# tracking remote branches 
git fetch origin remoteBranchName:localBranchName && git checkout 
#equivalent to
git checkout --track origin/theBranchName
# after this you pull

# your branch sucks your work is meaningless and youre behind
1/
git merge branchName -s recursive -Xtheirs # 
git merge --strategy recursive --strategy-option theirs # long options
2/
git push origin --delete Feat/12345 # deletes remote branch Feat/12345
git branch -d test # to delete the local branch

### Pushing branch to remote ###
git push origin feature # pushing branch "feature" 
git push --set-upstream origin feature # for creating branch on the remote

### lifesaver ###
git switch -c newBranch # switch to a new branch & take your staged/unstaged changes with you

### undoing a rebase
git reflog
git reset HEAD@{2} --hard

```

---

---
   
## Checkout commits & Detached HEAD

Checkout commits is like time travel, its useful when you wanna experiment with old revisions.

```powershell
git checkout 1a2b3c4
```

Under the hood, instead of moving the HEAD pointer it creates whats called `Detached HEAD` and point it to that commit.

Beware that when you make changes and commit them, those changes do not belong to any branch, and you risk losing your changes permanetly if you checkout, `unless you create a branch`.

---

---

## Merge Strategies (combining work)

**TLDR**; Most of the time, you'll be doing fast forward merges with squash (squash(group) useless feature commits) , and to do so you need to rebase the feature branch onto develop.

**_fast forward merge_** is performed when there is a direct linear path from the source branch to the target branch. In this case only the `HEAD` pointer is moved.

ps: only rebase branches that you created locally, dont rebase remote branches

**_3 way merge_** duh

```powershell
git rebase develop feature
git checkout develop
git merge feature
```

---

## Solving merge conflicts

...

---

## Gitignore

You can tell git to explicitly ignore files by adding them to [**.gitignore**](https://www.atlassian.com/git/tutorials/saving-changes/gitignore) file
which should be located in the root folder.

### Special things you can do include:

- Define patterns instead of prefixed folder/file names (eg: file extensions..)
- Define multiple gitignore files in subdirectories
- global gitignore rules
- Commiting an ignored file (using `git add -f` )
- Stash an ignored file (using `--all` flag)
- debug .gitignore files (what causes a file to be ignored) eg:
  ```bash
  git check-ignore -v path/to/ignored/file
  ```

---

## keywords and research material

- bisect (walktrough over commits while investigating bugs)
- git hooks (eg running tests before commits) ([git hooks](https://git-scm.com/docs/githooks), [husky](https://typicode.github.io/husky/), [pre-commit](https://pre-commit.com/) )
- [log subsets](https://git-scm.com/docs/git-log) :fearful:

---

## Resources:

- [Pro Git (pdf)](https://git-scm.com/book/en/v2)

- [[g]old](https://www.youtube.com/watch?v=ZDR433b0HJY)

- [Exaustive youtube playlist](https://www.youtube.com/playlist?list=PL_RrEj88onS-SAZOGnaUlexOzgRqWrOPR)

- [Atlassian](https://www.atlassian.com/git/tutorials/syncing)

- [learngitbranching (game)](https://learngitbranching.js.org/)
- [git-school (game)](https://git-school.github.io/visualizing-git/)
- [x-hub guide](https://drive.google.com/file/d/1tGvBGXmgC95BQUyhZaFadysBE-Scpl6h/view?usp=sharing)

- [oh shit git](https://ohshitgit.com/)
